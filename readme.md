* npm install -g cordova
* npm install -g ionic

* ionic start ionic2-e2e-example
* cd ionic2-e2e-example

* npm install

* npm install protractor --save-dev
* npm install -g webdriver-manager
* webdriver-manager update
* npm install jasmine --save-dev
* npm install jasmine-spec-reporter --save-dev
* npm install ts-node --save-dev
* npm install connect --save-dev
* npm install @types/jasmine --save-dev
* npm install @types/node --save-dev

* ionic serve

* protractor
* npm install @compodoc/compodoc --save